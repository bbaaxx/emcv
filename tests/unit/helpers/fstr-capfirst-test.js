import {
  fstrCapfirst
} from 'emcv/helpers/fstr-capfirst';

module('FstrCapfirstHelper');

// Replace this with your real tests.
test('it works', function() {
  var result = fstrCapfirst('dude');
  ok(result);
});
