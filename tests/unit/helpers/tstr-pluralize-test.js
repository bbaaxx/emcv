import {
  tstrPluralize
} from 'emcv/helpers/tstr-pluralize';

module('TstrPluralizeHelper');

// Replace this with your real tests.
test('it works', function() {
  var result = tstrPluralize('dude');
  ok(result);
});
